# DB structure:
# indexedDB: transports
# object store: places, it contains the lines and the stop area, it is just a cache


datetime = () ->
	d = new Date()
	day = d.getDate()
	hours = d.getHours()
	minutes = d.getMinutes().toString()
	month = d.getMonth() + 1
	if day < 10
		day = "0" + day.toString()
	else
		day = day.toString()
	if month < 10
		month = "0" + month.toString()
	else
		month = month.toString()
	if hours < 10
		hours = "0" + hours.toString()
	else
		hours = hours.toString()
	if minutes < 10
		minutes = "0" + minutes.toString()
	else
		minutes = minutes.toString()
	return d.getFullYear().toString() + month + day + "T" + hours + minutes

# int of seconds
duration_to_str = (n) ->
	minute = Math.floor(n/60)
	hours = Math.floor(minute/60)
	minute = minute % 60

	if hours > 0
		hours.toString() + "h " + minute.toString() + "min"
	else
		minute.toString() + "min"

datetime_to_str = (str) ->
	year = parseInt(str[0..3])
	month = parseInt(str[4..5])
	date = parseInt(str[6..7])
	hour = parseInt(str[9..10])
	minute = parseInt(str[11..12])

	d = new Date()
	formated = ""
	long = false

	if year != d.getFullYear()
		formated += date.toString() + "/" + month.toString() + "/" + year.toString() + " "
		long = true
	else if date != d.getDate() or month != d.getDate() + 1
		formated += date.toString() + "/" + month.toString() + " "
		long = true
		

	if minute < 10
		minute = "0" + minute.toString()
	else
		minute = minute.toString()
	formated += hour.toString() + "h" + minute
	[formated, long]

db = null

do_api_call = (path, done) ->
	region = "fr-idf"
	host = "https://api.navitia.io/v1/coverage/"
	auth = "Basic MzNkMTkxYWEtOTI4OC00ODY1LTllOWEtYWIyODE0MGM2YmRlOg=="
	xhr = new XMLHttpRequest()
	xhr.open("GET", host + region + "/" + path)
	xhr.setRequestHeader('Authorization', auth)
	xhr.onreadystatechange = (e) ->
		if xhr.readyState == 4
			done(xhr.responseText)
	xhr.send()

api_prefix = () ->
	region = "fr-idf"
	host = "https://api.navitia.io/v1/coverage/"
	host + region + "/"

do_raw_api_call = (path, done) ->
	auth = "Basic MzNkMTkxYWEtOTI4OC00ODY1LTllOWEtYWIyODE0MGM2YmRlOg=="
	xhr = new XMLHttpRequest()
	xhr.open("GET", path)
	xhr.setRequestHeader('Authorization', auth)
	xhr.onreadystatechange = (e) ->
		if xhr.readyState == 4
			done(xhr.responseText)
	xhr.send()


create_db = (done) ->
	db_request = window.indexedDB.open("transports", 5)
	db_request.onsuccess = (e) ->
		db = e.target.result
		console.log("connected to db")
		console.log(db)
		done()
	db_request.onupgradeneeded = (e) ->
		db = e.target.result
		console.log("upgrading db")

		places_store = db.createObjectStore("places", {keyPath : "navitia_id"})
		places_store.createIndex("name", "name", {unique:false})
		places_store.createIndex("navitia_id", "navitia_id", {unique:true})
		
		journeys_store = db.createObjectStore("journeys", {keyPath : "from_to_departure"})
		journeys_store.createIndex("from_to_departure", "from_to_departure", {unique:true})
		journeys_store.createIndex("from", "from", {unique:false})
		journeys_store.createIndex("to", "to", {unique:false})
		journeys_store.createIndex("arrival_date_time", "arrival_date_time", {unique:false})
		journeys_store.createIndex("departure_date_time", "departure_date_time", {unique:false})
		journeys_store.createIndex("saved", "saved", {unique:false})
		journeys_store.createIndex("sections", "sections", {unique:false})

add_place = (id, place) ->
	places_store = db.transaction("places", "readwrite").objectStore("places")
	places_store.put({navitia_id:id, name:place})

journey_get_id = (from, to, journey) ->
	return from + "/" + to + "/" + journey.departure_date_time

# async
add_journey = (from, to, journey, done) ->
	id = journey_get_id from, to, journey

	journeys_store = db.transaction("journeys", "readwrite").objectStore("journeys")
	journey_in_db =
		from_to_departure: id
		from: from
		to: to
		arrival_date_time: journey.arrival_date_time
		departure_date_time: journey.departure_date_time
		saved:journey.saved
		sections:journey.sections
		duration:journey.duration
	req = journeys_store.put(journey_in_db)
	req.onsuccess = done

view_journey = (id) ->
	journeys_store = db.transaction("journeys").objectStore("journeys")
	connect = journeys_store.get(name)
	connect.onsuccess = (e) ->
		if connect.result
			display_journey connect.result
		else
			console.log "unknown journey" + id

debug_print_db = (table, index) ->
	places_store = db.transaction(table).objectStore(table)
	
	searcher = places_store.index(index)
	console.log("done")
	searcher.openCursor().onsuccess = (e) ->
		c = e.target.result
		if c
			console.log(c.value)
			c.continue()
		else
			console.log("done")
	return 0


debug_print_places = () ->
	places_store = db.transaction("places").objectStore("places")
	
	searcher = places_store.index("navitia_id")
	console.log("done")
	searcher.openCursor().onsuccess = (e) ->
		c = e.target.result
		if c
			console.log(c.value)
			c.continue()
		else
			console.log("done")
	return 0

#async
get_place = (name, done) ->
	places_store = db.transaction("places").objectStore("places")
	connect = places_store.get(name)
	connect.onsuccess = (e) ->
		done(connect.result)

#async
get_journey = (name, done) ->
	places_store = db.transaction("journeys").objectStore("journeys")
	connect = places_store.get(name)
	connect.onsuccess = (e) ->
		done(connect.result)


#async
get_line_name = (name, done) ->
	places_store = db.transaction("places").objectStore("places")
	
	connect = places_store.get(name)
	connect.onsuccess = (e) ->
		if connect.result
			done(connect.result.name)
		else
			console.log name
			do_api_call "lines/" + name, (res) ->
				obj = JSON.parse res
				if obj.lines and obj.lines.length > 0
					add_place name, obj.lines[0].code
					done(obj.lines[0].code)
				else
					console.log "invalid line name" + name


# async
# from and to are stop_areas
# done(success, res)
query_journey = (from, to, done, count = 3) ->
	query_journey_aux = (from, to, done, url, count) ->
		console.log "querying journey"
		if count > 0
			do_raw_api_call url, (res) ->
				res_parsed = JSON.parse res
				if res_parsed.journeys
					i = 0
					res_parsed.journeys.forEach (l) ->
						done true, l
						i += 1
					if res_parsed.links and res_parsed.links[1]
						link = res_parsed.links[1]
						query_journey_aux from, to, done, link.href, count - i
				else
					done false, null
	query_journey_aux from, to, done, api_prefix() + "journeys?from=" + from + "&to=" + to + "&datetime=" + datetime(), count

compute_itineraire = () ->
	from_input = document.getElementById "transpo-from"
	to_input = document.getElementById "transpo-to"


	if from_input.place and to_input.place
		document.getElementById("transpo-itineraire-progress").classList.add("visible")
		ul = document.getElementById "itineraire_long"
		from = from_input.place.navitia_id
		to = to_input.place.navitia_id
	
		ul = document.getElementById "itineraire"
		ul.innerHTML = ""

		query_journey from, to, (ok, res) ->
			if ok
				add_journey from, to, res
				show_journey_entry from, to, (journey_get_id from, to, res), res
			else
				console.log "pas d'itinéraire"
			document.getElementById("transpo-itineraire-progress").classList.remove("visible")
	
	else if from_input.place
		complete("transpo-to")
	else if to_input.place
		complete("transpo-from")
	else
		complete("transpo-to")
		complete("transpo-from")

show_journey_entry = (from, to, id, res) ->
	ul = document.getElementById "itineraire"
	ul.innerHTML += "<li><a href='#' class='menu-item' onclick='show_journey(\"" + id + "\")'><p>" + (datetime_to_str res.departure_date_time)[0] + ":" + (duration_to_str res.duration) + "</p></a></li>"

show_journey = (id) ->
	get_journey id, (res) ->
		display_journey res
		hide_main()

save_journey = () ->
	ul = document.getElementById "itineraire_long"
	ul.journey.saved = true
	add_journey ul.journey.from, ul.journey.to, ul.journey, update_sidebar

update_sidebar = () ->
	list = (document.getElementById "saved_journeys")
	list.innerHTML = ""
	table = "journeys"
	index = "from_to_departure"
	journeys_store = db.transaction(table).objectStore(table)
	
	searcher = journeys_store.index(index)
	console.log("done")
	searcher.openCursor().onsuccess = (e) ->
		c = e.target.result
		if c
			if c.value.saved
				id = c.value.from_to_departure
				from = c.value.from
				to = c.value.to
				date = (datetime_to_str c.value.departure_date_time)[0]
				get_place from, (from) ->
					get_place to, (to) ->
						list.innerHTML += "<li><a href='#' onclick='javascript:show_journey(\"" + id + "\")'>" + date + " :<br />" + from.name + " vers " + to.name + "</a></li>"
				c.continue()
			else
				console.log c.value
				
				c.continue()
		else
			console.log("done")


	
display_journey = (res) ->
	ul = document.getElementById "itineraire_long"
	ul.innerHTML = ""
	ul.journey = res

	from = res.from
	to = res.to
	(document.querySelector "#view_itineraire h1").innerHTML = (datetime_to_str res.departure_date_time)[0]
	get_place from, (from) ->
		get_place to, (to) ->
			(document.querySelector "#view_itineraire > p").innerHTML = from.name + " vers " + to.name
	
	res.sections.forEach (s) ->
		if s.duration > 0
			c = ""
			c += "<li class ='transpo-item transpo-" + s.type.replace("_", "-") + "'><ul><li id='" + s.id + "-line' class='transpo-item-line'></li>"
			
			if s.type == "public_transport" or s.type == "street_network"
				c +=  "<li class='transpo-item-general'><p>"
				c += "De : "
				c += s.from.name
				c += "<br />"
				c += "À : "
				c += s.to.name
			else if s.type == "waiting"
				c +=  "<li class='transpo-item-general'><p>"
				c += "Attente"
			else if s.type == "crow_fly"
				c +=  "<li class='transpo-item-general'><p>"
				c += "Marche à pieds"
			c += "</p></li>"
			date1 =  (datetime_to_str s.departure_date_time)
			date2 =  (datetime_to_str s.arrival_date_time)
			if date1[1] or date2[1]
				c += "<li class='transpo-item-hour-long'>"
			else
				c += "<li>"
			c += "<p>" + date1[0] + "<br />"
			c += date2[0] + "</p></li>"
			c += "</ul></li>"
			ul.innerHTML += c
			s.links.forEach (l) ->
				if l.type == "line"
					get_line_name l.id, (n) ->
						(document.getElementById s.id + "-line").innerHTML = "<p>" + n + "</p>"
	
	if res.saved
		ul.innerHTML += "<li><button onclick='discard_journey(\"" + res.from_to_departure + "\")'>Enlever</button></li>"
	else
		console.log res

complete_with_stop_area = (id, input_name) ->
	get_place id, (pl) ->
		input = document.getElementById input_name
		input.value = pl.name
		input.place = pl
		input.setAttribute("readonly", true)
		
		compl = document.getElementById input_name + "-completion"
		compl.classList.add("hidden")
	return null

complete_reset = (input_name) ->
	input = document.getElementById input_name
	input.removeAttribute("readonly")
	input.value = null
	input.place = null
	

complete = (input_name) ->
	input = document.getElementById input_name
	debut = input.value
	to = document.getElementById input_name + "-completion"
	to.innerHTML = ""
	to.classList.remove("hidden")
	console.log input_name + "-completion-progress"
	document.getElementById(input_name + "-completion-progress").classList.add("visible")
	do_api_call("places/?count=10&q=" + debut, (res) ->
		document.getElementById(input_name + "-completion-progress").classList.remove("visible")
		obj = JSON.parse res
		obj.places.forEach (p) ->
			console.log (Object.keys p)
			if p.embedded_type == "stop_area"
				add_place p.id, p.name
				to.innerHTML += "<li class='list_stop_areas'><a href='#' onclick='javascript:complete_with_stop_area(\"" + p.id + "\", \"" + input_name + "\")'><p>" + p.name + "</p></a></li>"
		input.scrollIntoView({block:'end',behavior:'smooth'})
	)


install_completion = (input_name) ->
	obj = document.getElementById input_name
	obj.oninput = () ->
		obj.scrollIntoView({block:'end', behavior:'smooth'})
		if obj.inputtimeout
			clearTimeout(obj.inputtimeout)
		if !obj.place
			obj.inputtimeout = setTimeout(() ->
				complete(input_name)
			, 500)

install_completion "transpo-to"
install_completion "transpo-from"


"
setTimeout (() ->
	display_itineraire (JSON.parse journey).journeys[0]
), 1000
"

create_db () ->
	console.log "db loaded"
	update_sidebar()



show_main = () ->
	document.getElementById("main_section").classList.add("current")
	document.getElementById("view_itineraire").classList.remove("current")

hide_main = () ->
	document.getElementById("main_section").classList.remove("current")
	document.getElementById("main_section").classList.add("previous")
	document.getElementById("view_itineraire").classList.add("current")

discard_journey = (id) ->
	get_journey id, (res) ->
		res.saved = false
		add_journey res.from, res.to, res, update_sidebar
		show_main()
